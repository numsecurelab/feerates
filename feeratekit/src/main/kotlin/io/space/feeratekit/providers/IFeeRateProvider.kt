package io.space.feeratekit.providers

import io.space.feeratekit.model.FeeRate
import io.reactivex.Single


interface IFeeRateProvider {
    fun getFeeRates(): Single<FeeRate>
}