package io.space.feeratekit

import io.space.feeratekit.model.Coin
import io.space.feeratekit.model.FeeRate

interface IStorage {
    fun getFeeRate(coin: Coin): FeeRate?
    fun setFeeRate(rate: FeeRate)
}
