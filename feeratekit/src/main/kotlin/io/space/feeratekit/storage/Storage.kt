package io.space.feeratekit.storage

import io.space.feeratekit.model.Coin
import io.space.feeratekit.model.FeeRate
import io.space.feeratekit.IStorage

class InMemoryStorage() : IStorage {

    private var ratesDb: MutableMap<Coin, FeeRate> = mutableMapOf()

    override fun getFeeRate(coin: Coin): FeeRate? {
        return ratesDb.get(coin)
    }

    override fun setFeeRate(rate: FeeRate) {
        ratesDb.set(rate.coin, rate)
    }

}
