package io.space.feeratekit

import android.content.Context
import io.space.feeratekit.model.Coin
import io.space.feeratekit.model.FeeProviderConfig
import io.space.feeratekit.model.FeeRate
import io.space.feeratekit.providers.FeeRateProviderManager
import io.space.feeratekit.storage.InMemoryStorage
import io.reactivex.Single
import io.reactivex.functions.Function4

class FeeRateKit(
    providerConfig: FeeProviderConfig,
    private val context: Context) {

    private val providerManager: FeeRateProviderManager

    init {
        providerManager = FeeRateProviderManager(providerConfig, InMemoryStorage())
    }

    fun bitcoin(): Single<FeeRate> {
        return getRate(Coin.BITCOIN)
    }

    fun bitcoinCash(): Single<FeeRate> {
        return getRate(Coin.BITCOIN_CASH)
    }

    fun dash(): Single<FeeRate> {
        return getRate(Coin.DASH)
    }

    fun ethereum(): Single<FeeRate> {
        return getRate(Coin.ETHEREUM)
    }

    fun getRate(coinCode: String): Single<FeeRate> {

        Coin.getCoinByCode(code = coinCode)?.also {
            return getRate(it)
        }

        throw IllegalArgumentException()
    }

    @Suppress("UNCHECKED_CAST")
    private fun getStatusData(coin: Coin): Single<Any> {

        return (getRate(coin) as Single<Any>).onErrorResumeNext {
            Single.just(Pair(coin.name, "Error:${it.localizedMessage}"))
        }
    }

    fun statusInfo(): Single<Map<String, Any>>? {

        return Single.zip(
            getStatusData(Coin.BITCOIN),
            getStatusData(Coin.ETHEREUM),
            getStatusData(Coin.BITCOIN_CASH),
            getStatusData(Coin.DASH),
            Function4<Any, Any, Any, Any, Array<Any>> { btcRate, ethRate, bchRate, dashRate ->
                arrayOf(btcRate, ethRate, bchRate, dashRate)
            })
            .map { rates ->

                val statusInfo = LinkedHashMap<String, Any>()

                for (rate in rates) {
                    if (rate::class == FeeRate::class) {
                        (rate as FeeRate).let {
                            statusInfo.put(
                                it.coin.name,
                                mapOf(
                                    Pair("HighPriority", it.highPriority),
                                    Pair("MediumPriority", it.mediumPriority),
                                    Pair("LowPriority", it.lowPriority)
                                )
                            )
                        }
                    } else statusInfo.plusAssign((rate as Pair<String,String>))
                }
                statusInfo
            }
    }

    private fun getRate(coin: Coin): Single<FeeRate> {
        val xxx = providerManager.getFeeRateProvider(coin).getFeeRates()
        return xxx
    }
}
